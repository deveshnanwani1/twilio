from twilio.rest import Client
from django.http import HttpResponse
from twilio.twiml.messaging_response import MessagingResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import action
from rest_framework.viewsets import GenericViewSet
from .models import Test


class SMSTwillo(GenericViewSet):
    queryset = Test.objects.all()

    @action(methods=['post'], detail=False, url_path='sms')
    def send_sms(self, request):
        print(request.data)
        from_ = request.data['from']
        to = request.data['to']
        body = request.data['message']
        # Your Account Sid and Auth Token from twilio.com/console
        # DANGER! This is insecure. See http://twil.io/secure
        account_sid = 'ACa04cf7c7ba488bcacb62a44fe88c8317'
        auth_token = '92b704195e71f92c5b4fbc3e8201745e'
        client = Client(account_sid, auth_token)

        message = client.messages \
            .create(
                body=body,
                from_=from_,
                to=to
            )
        return HttpResponse(f'message_id {message.sid}')

    @action(methods=['get', 'post'], detail=False, url_path='reply')
    def reply(self, request):
        # Start our response
        resp = MessagingResponse()

        receive = request.POST.get('Body')
        print(receive)
        str = f"message is {receive} Thanks so much for your message."
        # Add a message
        resp.message(str)

        return HttpResponse(f'receive_text {receive}', content_type='text/xml')
